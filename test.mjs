// Importer les classes Catalogue, Produit et Etagere depuis les fichiers correspondants
import {Catalogue} from './Catalogue.mjs';
import {Produit} from './Produit.mjs';
import {Etagere} from './Etagere.mjs';

// Créer un nouvel objet de type Catalogue avec le nom "Pokedev"
let pokedev = new Catalogue("Pokedev");

// Créer un nouvel objet de type Produit avec le nom "Pikachu" et un prix de 1000
let pikachu = new Produit("Pikachu", 1000);

// Ajouter le produit Pikachu au catalogue Pokedev
pokedev.ajouterProduit(pikachu);

// Afficher le contenu du catalogue Pokedev
pokedev.afficher();

// Ajouter de nouveau le produit Pikachu au catalogue Pokedev
pokedev.ajouterProduit(pikachu);

// Afficher à nouveau le contenu du catalogue Pokedev, qui doit maintenant contenir un seul Pikachu grace au set()
pokedev.afficher();

// Créer un nouvel objet de type Etagere avec le label "etagePoke"
let etagePoke = new Etagere("etagePoke");

// Ajouter 5 Pikachu à l'étagère etagePoke
etagePoke.ajouterProduits("Pikachu", 5);

// Afficher le contenu de l'étagère etagePoke, qui doit contenir 5 Pikachu
console.log(etagePoke);

// Ajouter 15 Pikachu à l'étagère etagePoke
etagePoke.ajouterProduits("Pikachu", 15);

// Afficher à nouveau le contenu de l'étagère etagePoke, qui doit maintenant contenir 5 Pikachu (car la quantité maximale est fixée à 10)
console.log(etagePoke);
