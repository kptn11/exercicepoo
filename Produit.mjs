// Importer les classes Catalogue et Etagere depuis les fichiers Catalogue.mjs et Etagere.mjs
import { Catalogue } from './Catalogue.mjs';
import { Etagere } from './Etagere.mjs';

// Définir la classe Produit
export class Produit {
    // Définir le constructeur de la classe, qui prend le nom et le prix du produit en tant que paramètres
    constructor(nom, prix){
        // Attribuer le nom du produit à l'attribut nom
        this.nom = nom;
        // Attribuer le prix du produit à l'attribut prix
        this.prix = prix;
    }
}
