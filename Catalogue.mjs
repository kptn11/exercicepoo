// Importer la classe Produit depuis le fichier Produit.mjs
import {Produit} from './Produit.mjs';

// Définir la classe Catalogue
export class Catalogue{
    // Définir le constructeur de la classe, qui prend le nom du catalogue en tant que paramètre
    constructor(nom){
        // Attribuer le nom du catalogue à l'attribut nom
        this.nom = nom;
        // Initialiser l'ensemble de produits vide
        //Le Set() permet de limiter les nombre de produit a un seul exemplaire par produit
        this.produits = new Set();
    }

    // Définir une méthode pour ajouter un produit au catalogue
    ajouterProduit(produit){
        // Ajouter le produit fourni à l'ensemble de produits du catalogue
        this.produits.add(produit);        
    }

    // Définir une méthode pour afficher les informations sur le catalogue et les produits qu'il contient
    afficher(){
        // Afficher le nom du catalogue
        console.log(`
        Nom: ${this.nom}
        Produits :`)
        // Afficher tous les produits dans l'ensemble de produits
        console.log(this.produits);
        // Pour chaque produit, afficher son nom
        for(let p of this.produits){
            console.log(`           - ${p.nom}`);
        }
    }
}
