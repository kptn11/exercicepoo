// Importer la classe Produit depuis le fichier Produit.mjs
import { Produit } from "./Produit.mjs";

// Définir la classe Etagere
export class Etagere{
    // Définir le constructeur de la classe, qui prend un label (étiquette) en tant que paramètre
    constructor(label){
        // Attribuer l'étiquette à l'attribut label
        this.label = label;
        // Initialiser un tableau vide pour stocker les produits de l'étagère
        //Le tableau permet d'avoir plusieur fois le même produit sur l'etagere
        this.produits = [];
    }
    
    // Définir une méthode pour ajouter des produits à l'étagère
    ajouterProduits(produit, qt){
        //Si la quantité est < a la limite
        if(qt<10){
            //Alors
            // Ajouter le produit à la fin du tableau produits
            this.produits.push(produit);
            // Ajouter la quantité de produits à la fin du tableau produits
            this.produits.push(qt);
        }else{
            //Sinon
            console.log("Il n'y a que 10 places disponibles ");
        }
    }
}
